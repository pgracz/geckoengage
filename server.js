"use strict";

/* eslint-disable no-sync, no-process-env, no-undefined */

// Libraries
const polka = require('polka');
const log4js = require('log4js');

// Config
const Config = require('./server/Config');
const config = Config.getMergedConfig('./config/config.production.yaml');

//
const Webserver = require('./server/http/webserver');
const Router = require('./server/http/router');
const BasicController = require('./server/controllers/basic.controller');
const PlanetController = require('./server/controllers/planet.controller');

const DbStorage = require('./server/storageService/DbStorage');
const PlanetModel = require('./server/models/PlanetModel');

// Logging
log4js.configure(config.logging);


// HTTP
const webServer = new Webserver(polka, log4js);
const router = new Router(
    webServer,
    log4js,
    [
        new BasicController(config, log4js),
        new PlanetController(config, log4js, new PlanetModel(new DbStorage(config, log4js), log4js))
    ]
);

router.start(config.outputs.http_server.port);
