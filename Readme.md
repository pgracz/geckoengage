Hello!

Please find the repo for the project (its public for a short time!):
```https://gitlab.com/pgracz/geckoengage```

The app only allows request of application-json content type.

The available routes, as well as bindings, are configured via yaml config.
The project was build using gitlab CI (see .gitlab-ci.yaml).

To run on you machine:
```
docker-compose -f ./docker/dev.yaml up --build
```
Docker and docker-compose will be necessary to run the above.

If you want to run the api test on the dev env run :
```
docker-compose -f ./docker/dev.test.yaml up
```

Once the dev container is up and running. It will plug into the docker bridge network and make some http requests.

To run linter:
```
npm run lint OR npm run lint:fix
```

To run unit tests:
``` npm run test```

Available env variables:
PORT -> allows changing under which port the http server will be available (godsend in k8s)
LOG_LEVEL -> sets log level for log4js

All endpoints are http/port 80

endpoints:
POST /create
On successful creation will return correctly created record,

GET /getByUuid
eg:
localhost:80/getByUuid?id=6d1f658f-b367-4691-9b4b-78e859d6cd23

DELETE /deleteByUuid/:id
eg: localhost:80/deleteByUuid/546f7dda-0bfa-4233-8749-e819281f95e6

GET /getAll will return all records (no pagination!)

GET /getTotalWeight wil return an array with object sum (see integration/api tests for example)
eg: [
        {
            "sum(`weight`)": 747564
        }
    ]
    
I strongly advice using postman for interactions with the api. It isn't perfect, but I liked to learn knex with sqlite. Error handling could be improved.
