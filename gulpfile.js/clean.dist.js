"use strict";

const del = require('del');
const path = require('path');

module.exports = function cleanDist (cb) {
    del([path.join(__dirname, '..', 'dist')])
        .then(() => {
            cb();
        });
};
