"use strict";

const gulp = require('gulp');
const path = require('path');

module.exports = function copyFiles () {
    const join = (...file) => {
        return path.join(__dirname, '..', ...file);
    };

    const source = [
        join('package.json'),
        join('package-lock.json'),
        join('docker-entrypoint.sh'),
        join('Dockerfile'),
        join('server.js'),
        join('config', '*.yaml'),
        join('server', '**', '*'),
        join('db', '**', '*'),
    ];

    const dest = path.join(__dirname, '..', 'dist');
    return gulp.src(source, {base: './'}).pipe(gulp.dest(dest));
};
