"use strict";

const fs = require('fs');
const path = require('path');

module.exports = function updateVersion (cb) {
    const dist = path.join(__dirname, '..', 'dist', 'package.json');
    const packageJson = require(dist);

    const pointer = 1;
    const number = process.argv.indexOf("--number");

    if (number > -1) {

        // Define the package version with the supplied number value, strip preceding "v" if it exists
        const version = process.argv[number + pointer].replace(/[vV]/g, '');

        // Update version in the dist package.json and package-lock.json files
        packageJson.version = version;

        // Write back the updated files
        fs.writeFile(dist, JSON.stringify(packageJson, null, 2), (err) => {
            if (err) {
                return console.log(err);
            }

            console.log('Version updated to ' + version + ' in: ' + dist);
            cb();
        });
    } else {
        cb(new Error('Version not specified - use "--number 0.0.0" in command'));
    }
};
