"use strict";

const gulp = require('gulp');

const cleanDist = require('./clean.dist');
const copyFiles = require('./copy.files');
const updateVersion = require('./update.version');

exports.clean = cleanDist;
exports.copyFiles = copyFiles;
exports.updateVersion = updateVersion;

exports.preparepublish = gulp.series(
    cleanDist,
    copyFiles,
    updateVersion
);

exports.default = exports.preparepublish;
