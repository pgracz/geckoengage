exports.up = function(knex, Promise) {
    return knex.schema.createTable('planets', function(t) {
        t.uuid('id').primary()
        t.dateTime('createdAt').notNull();
        t.dateTime('updatedAt').nullable();
        t.dateTime('deletedAt').nullable();

        t.string('name').notNull();
        t.string('system').notNull();
        t.text('description').nullable();
        t.decimal('weight', 6, 2).notNull();
        t.enum('category', ['planet', 'star', 'moon', 'asteroid', 'other']).notNull();
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('planets');
};
