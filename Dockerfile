FROM node:12-alpine as preinstall
LABEL name="npm ci stage"
WORKDIR /app
ADD package.json package-lock.json /app/
RUN npm ci --production

FROM node:12-alpine as copyfiles
LABEL name="copy files stage"
WORKDIR /app
ADD . /app
RUN rm -rf /app/node_modules

FROM node:12-alpine
LABEL name="GeckoEngage Sample"
LABEL maintainer="Peter Gracz <gracz.peter@gmail.com>"

ENTRYPOINT ["./docker-entrypoint.sh"]
WORKDIR /app
CMD ["npm", "start"]
ENV NODE_ENV=production

COPY --from=copyfiles /app /app
COPY --from=preinstall /app/node_modules /app/node_modules
