'use strict'

const appJsonType = require(__basedir + '/server/http/middleware/appJsonType.middleware');
const dropEmpties = require(__basedir + '/server/http/middleware/dropEmpties.middleware');

test('try appJsonType middleware', () => {
    const res = {};
    const req = {
        headers: {'content-type': 'application/json'}
    };
    const next = jest.fn();
    appJsonType(req,res, next);
    expect(next).toHaveBeenCalled();

    const res1 = {
        end: jest.fn()
    };
    const req1 = {
        headers: {'content-type': 'text/yaml'}
    };
    appJsonType(req1,res1, next);
    expect(res1.statusCode).toEqual(400);
    expect(res1.end).toHaveBeenCalledWith('Wrong content type');
});


test('try dropEmpties middleware', () => {
    const res = {};
    const req = {
        body: 'I am a body, and not empty at all'
    };
    const next = jest.fn();
    dropEmpties(req,res, next);
    expect(next).toHaveBeenCalled();

    const res1 = {
        end: jest.fn()
    };
    const req1 = {
        body: null
    };
    dropEmpties(req1,res1, next);
    expect(res1.statusCode).toEqual(400);
    expect(res1.end).toHaveBeenCalledWith('No body!');
});
