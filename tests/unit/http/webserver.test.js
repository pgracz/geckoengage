"use strict";

const Webserver = require(__basedir + '/server/http/webserver');
const log = {
    getLogger: jest.fn().mockImplementation(() => {
        return {
            log: jest.fn(),
            trace: jest.fn(),
            debug: jest.fn(),
            info: jest.fn(),
            warn: jest.fn(),
            error: jest.fn(),
            fatal: jest.fn()
        };
    })
};


const mockPolka = jest.fn().mockReturnValue({
    handler: jest.fn(),
    use: jest.fn()
});

const fakeServer = function fakeServer(broken) {
    return (handler) => {
        let innerFakeServer = {
            once: jest.fn().mockImplementation((errorEventName, errorCallback) => {
                if (errorEventName === 'error') {
                    innerFakeServer.errorCallback = errorCallback;
                }
                return innerFakeServer;
            }),
            close: jest.fn(),
            listen: jest.fn().mockImplementation((port, ip, callback) => {
                if (!!broken) {
                    let error = new Error('Error Event Called');
                    error.server = innerFakeServer;
                    innerFakeServer.errorCallback(error);
                } else {
                    return callback();
                }
            }),
        };
        return innerFakeServer;
    };
};

test('start returns fakeServer with handler', () => {
    const web = new Webserver(mockPolka, log);
    web.createServer = fakeServer(false);

    expect.assertions(3);
    return web.start(12345).then((returnedServer) => {
        expect(returnedServer.once).toHaveBeenCalledWith('error', expect.anything());
        expect(returnedServer.close).not.toHaveBeenCalled();
        expect(returnedServer.listen).toHaveBeenCalledWith(12345, '0.0.0.0', expect.anything());
    });
});

test('start errors', () => {
    const web = new Webserver(mockPolka, log);
    web.createServer = fakeServer(true);

    expect.assertions(4);
    return web.start().catch((err) => {
        // For fake, we reject an error with the innerFakeServer attached
        expect(err.message).toEqual('Error Event Called');
        expect(err.server.once).toHaveBeenCalledWith('error', expect.anything());
        expect(err.server.close).toHaveBeenCalled();
        expect(err.server.listen).toHaveBeenCalledWith(80, '0.0.0.0', expect.anything());
    });
});

test('use() proxy', () => {
    const innerPolka = {get: jest.fn(), use: jest.fn()};
    const mockPolka = () => innerPolka;
    const web = new Webserver(mockPolka, log);

    web.use('foo', 'foo');
    expect(innerPolka.use).toHaveBeenCalledWith('foo', 'foo');
});

test('getServer', () => {
    const innerPolka = {use: jest.fn()};
    const mockPolka = () => innerPolka;
    const web = new Webserver(mockPolka, log);
    expect(web.getServer()).toStrictEqual(innerPolka);
});
