'use strict'

const PlanetModel = require(__basedir + '/server/models/PlanetModel');

let logger = {
    getLogger: jest.fn().mockReturnValue({debug: jest.fn()})
};

const storageService = {
    store: jest.fn(),
    getByUuid: jest.fn().mockResolvedValue('MMMMMMM delicious value'),
    deleteByUuid: jest.fn(),
    update: jest.fn(),
    getAll: jest.fn(),
    getSum: jest.fn(),
}

test("Assert that create maps to store in storage service ", () => {
    const model = new PlanetModel(storageService, logger);

    let newPlanet = {
        name: 'newPlanet.name',
        system: 'newPlanet.system',
        description: 'newPlanet.description',
        weight: 'newPlanet.weight',
        category: 'newPlanet.category',
    };
    model.create(newPlanet)
    expect(model.storageService.store).toHaveBeenCalledWith('planets', expect.objectContaining(newPlanet));
});

test("Assert that retrieve maps to store in getByUuid service ", () => {
    const model = new PlanetModel(storageService, logger);

    model.retrieve('some uuid')
    expect(model.storageService.getByUuid).toHaveBeenCalledWith('planets', 'some uuid');
});

test("Assert that erase maps to deleteByUuid in getByUuid service ", () => {
    const model = new PlanetModel(storageService, logger);

    model.erase('some uuid2')
    expect(model.storageService.deleteByUuid).toHaveBeenCalledWith('planets', 'some uuid2');
});

test("Assert that updateRecord maps to update in update service ", () => {
    const model = new PlanetModel(storageService, logger);
    let updatedPlanet = {
        name: 'newPlanet.name1',
        system: 'newPlanet.system1',
        description: 'newPlanet.description1',
        weight: 'newPlanet.weight1',
        category: 'newPlanet.category1',
    };
    model.updateRecord(updatedPlanet)
    expect(model.storageService.update).toHaveBeenCalledWith('planets', expect.objectContaining(updatedPlanet));
});

test("Assert that retrieveAll maps to getAll in update service ", () => {
    const model = new PlanetModel(storageService, logger);

    model.retrieveAll()
    expect(model.storageService.getAll).toHaveBeenCalledWith('planets');
});

test("Assert that getTotalWeight maps to getSum in update service ", () => {
    const model = new PlanetModel(storageService, logger);

    model.getTotalWeight()
    expect(model.storageService.getSum).toHaveBeenCalledWith('planets', 'weight');
});
