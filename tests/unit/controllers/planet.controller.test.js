const PlanetController = require(__basedir + '/server/controllers/planet.controller');

test("Constructor test, ensure that the properties are set in the class", () => {
    let planetModel = {};
    const config = {
        outputs: {
            routesMap: {
                'PlanetController': [
                    {
                        'method': "post",
                        'route': "/create",
                        'function': "addCelestialBody"
                    },
                    {
                        'method': "get",
                        'route': "/getByUuid",
                        'function': "getCelestialBodyByUuid"
                    },
                ]
            }
        }
    }
    logger = {};
    logger.getLogger = jest.fn();
    let controller = new PlanetController(config, logger, planetModel);
    expect(logger.getLogger).toHaveBeenCalledWith('PlanetController');
    expect(controller.planetModel).toEqual(planetModel);
});

[
    [
        {
            'methodError': "post",
            'route': "/create",
            'function': "addCelestialBody"
        },
        'method is a required parameter to create controller route.'
    ],
    [
        {
            'method': "get",
            'routeError': "/getByUuid",
            'function': "getCelestialBodyByUuid"
        },
        'route is a required parameter to create controller route.'
    ],
    [
        {
            'method': "get",
            'route': "/getByUuid",
            'functionError': "getCelestialBodyByUuid"
        },
        'function is a required parameter to create controller route.'
    ],
    [
        undefined,
        "Config cannot be empty!"
    ],
].forEach((configItem) => {
    test("Constructor test, the parent class does the job in terms of registering the routes / throwing", () => {
        let planetModel = {};
        const config = {
            outputs: {
                routesMap: {
                    'PlanetController': [configItem[0]]
                }
            }
        }
        logger = {};
        logger.getLogger = jest.fn();
        const test = () => {
            new PlanetController(config, logger, planetModel);
        };
        expect(test).toThrowError(configItem[1]);
    });
});

test("Assert that addCelestialBody maps to create in model and calls output", () => {
    let planetModel = {};
    planetModel.create = jest.fn().mockResolvedValue({'some': "value"})
    const config = {
        outputs: {
            routesMap: {
                'PlanetController': [{
                    'method': "get",
                    'route': "/getByUuid",
                    'function': "getCelestialBodyByUuid"
                }]
            }
        }
    }
    let logger = {};
    logger.getLogger = jest.fn().mockReturnValue({debug: jest.fn()});
    const controller = new PlanetController(config, logger, planetModel);
    const req = {
        body: "uuuu lalallala"
    }
    const res = {
        writeHead: jest.fn(),
        end: jest.fn()
    };

    controller.addCelestialBody(req, res).then(() => {
        expect(res.end).toHaveBeenCalledWith(JSON.stringify({'some': "value"}));

    });
    expect(planetModel.create).toHaveBeenCalledWith("uuuu lalallala");
});

test("Assert that getCelestialBodyByUuid maps to retrieve in model and calls output", () => {
    let planetModel = {};
    planetModel.retrieve = jest.fn().mockResolvedValue({'some': "value"})
    const config = {
        outputs: {
            routesMap: {
                'PlanetController': [{
                    'method': "get",
                    'route': "/getByUuid",
                    'function': "getCelestialBodyByUuid"
                }]
            }
        }
    }
    let logger = {};
    logger.getLogger = jest.fn().mockReturnValue({debug: jest.fn()});
    const controller = new PlanetController(config, logger, planetModel);
    const req = {
        query: {
            id: "some ID"
        }
    }
    const res = {
        writeHead: jest.fn(),
        end: jest.fn()
    };

    controller.getCelestialBodyByUuid(req, res).then(() => {
        expect(res.end).toHaveBeenCalledWith(JSON.stringify({'some': "value"}));
    });
    expect(planetModel.retrieve).toHaveBeenCalledWith("some ID");
});

test("Assert that updateCelestialBody maps to updateRecord in model and calls output", () => {
    let planetModel = {};
    planetModel.updateRecord = jest.fn().mockResolvedValue({'some': "value"})
    const config = {
        outputs: {
            routesMap: {
                'PlanetController': [{
                    'method': "get",
                    'route': "/getByUuid",
                    'function': "getCelestialBodyByUuid"
                }]
            }
        }
    }
    let logger = {};
    logger.getLogger = jest.fn().mockReturnValue({debug: jest.fn()});
    const controller = new PlanetController(config, logger, planetModel);
    const req = {
        body: {
            dataaaaaa: "valueeeee"
        }
    }
    const res = {
        writeHead: jest.fn(),
        end: jest.fn()
    };

    controller.updateCelestialBody(req, res).then(() => {
        expect(res.end).toHaveBeenCalledWith(JSON.stringify({'some': "value"}));
    });
    expect(planetModel.updateRecord).toHaveBeenCalledWith(req.body);
});

test("Assert that deleteCelestialBody maps to erase in model and calls output", () => {
    let planetModel = {};
    planetModel.erase = jest.fn().mockResolvedValue({'some': "value"})
    const config = {
        outputs: {
            routesMap: {
                'PlanetController': [{
                    'method': "get",
                    'route': "/getByUuid",
                    'function': "getCelestialBodyByUuid"
                }]
            }
        }
    }
    let logger = {};
    logger.getLogger = jest.fn().mockReturnValue({debug: jest.fn()});
    const controller = new PlanetController(config, logger, planetModel);
    const req = {
        params: {
            id: "IDIDIDIDI"
        }
    }
    const res = {
        writeHead: jest.fn(),
        end: jest.fn()
    };

    controller.deleteCelestialBody(req, res).then(() => {
        expect(res.end).toHaveBeenCalledWith(JSON.stringify({'some': "value"}));
    });
    expect(planetModel.erase).toHaveBeenCalledWith(req.params.id);
});

test("Assert that getAllCelestialBodies maps to retrieveAll in model and calls output", () => {
    let planetModel = {};
    planetModel.retrieveAll = jest.fn().mockResolvedValue({'some': "value"})
    const config = {
        outputs: {
            routesMap: {
                'PlanetController': [{
                    'method': "get",
                    'route': "/getByUuid",
                    'function': "getCelestialBodyByUuid"
                }]
            }
        }
    }
    let logger = {};
    logger.getLogger = jest.fn().mockReturnValue({debug: jest.fn()});
    const controller = new PlanetController(config, logger, planetModel);
    const req = {}
    const res = {
        writeHead: jest.fn(),
        end: jest.fn()
    };

    controller.getAllCelestialBodies(req, res).then(() => {
        expect(res.end).toHaveBeenCalledWith(JSON.stringify({'some': "value"}));
    });
    expect(planetModel.retrieveAll).toHaveBeenCalled();
});

test("Assert that getTotalWeightOfCelestialBodies maps to getTotalWeight in model and calls output", () => {
    let planetModel = {};
    planetModel.getTotalWeight = jest.fn().mockResolvedValue(100)
    const config = {
        outputs: {
            routesMap: {
                'PlanetController': [{
                    'method': "get",
                    'route': "/getByUuid",
                    'function': "getCelestialBodyByUuid"
                }]
            }
        }
    }
    let logger = {};
    logger.getLogger = jest.fn().mockReturnValue({debug: jest.fn()});
    const controller = new PlanetController(config, logger, planetModel);
    const req = {}
    const res = {
        writeHead: jest.fn(),
        end: jest.fn()
    };

    controller.getTotalWeightOfCelestialBodies(req, res).then(() => {
        expect(res.end).toHaveBeenCalledWith(JSON.stringify(100));
    });
    expect(planetModel.getTotalWeight).toHaveBeenCalled();
});
