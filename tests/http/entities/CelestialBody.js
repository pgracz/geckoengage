"use strict";

const faker = require('faker');
const UUID_REGEX = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/;
const DATE_REGEX = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}\+00:00/

module.exports = class CelestialBody {
    constructor(test = true) {
        if (test) {
            this.id = expect.stringMatching(UUID_REGEX);
            this.createdAt = expect.stringMatching(DATE_REGEX);
            this.updatedAt = expect.stringMatching(DATE_REGEX);
        }
        this.name = faker.random.words(1);
        this.system = faker.random.words(1);
        this.description = faker.random.words(5)
        this.weight = faker.random.number();
        this.category = 'planet'
    }

    create(name, systemName, description, weight, category) {

        return {
            "name": name,
            "system": systemName,
            "description": description,
            "weight": weight,
            "category": category
        }
    }

    update(category) {
        this.category = category;
        return {
            name : this.name,
            system : this.system,
            description : this.description,
            weight : this.weight,
            category : this.category
        }
    }
};
