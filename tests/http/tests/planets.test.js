'use strict';

const request = require('../helpers/request');
const CelestialBody = require('../entities/CelestialBody');
const UUID_REGEX = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/;
const DATE_REGEX = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}\+00:00/

const celestialBody = new CelestialBody(false);
const host = 'dev-ms';

describe(`CelestialBody Tests`, () => {

    // Main Tests ================================================================
    describe(`Create new celestial body`, () => {
        test(`Successfully create a celestial body`, () => {
            return request(host).post('create')
                .payload(celestialBody)
                .send()
                .then((res) => {
                    celestialBody.id = res.body.id;
                    res.body.id = expect.stringMatching(UUID_REGEX);
                    res.body.createdAt = expect.stringMatching(DATE_REGEX);
                    expect(res.body.name).toEqual(celestialBody.name);
                    expect(res.body.system).toEqual(celestialBody.system);
                    expect(res.body.description).toEqual(celestialBody.description);
                    expect(res.body.weight).toEqual(celestialBody.weight);
                    expect(res.body.category).toEqual(celestialBody.category);
                });
        });
    });

    // ================================================================
    describe(`Get a celestial body`, () => {
        test(`Successfully retrieve the celestial body by uuid`, () => {
            return request(host).get('getByUuid' + '?id=' + celestialBody.id)
                .delay(500)
                .send()
                .then((res) => {
                    expect(res.body.id).toEqual(celestialBody.id);
                    res.body.id = expect.stringMatching(UUID_REGEX);
                    res.body.createdAt = expect.stringMatching(DATE_REGEX);
                    expect(res.body.name).toEqual(celestialBody.name);
                    expect(res.body.system).toEqual(celestialBody.system);
                    expect(res.body.description).toEqual(celestialBody.description);
                    expect(res.body.weight).toEqual(celestialBody.weight);
                    expect(res.body.category).toEqual(celestialBody.category);
                });
        });
    });

    // ================================================================

    describe(`Update the celestial body`, () => {
        test(`Successfully update the celestial body}`, () => {
            // Set new values to update
            celestialBody.category = 'moon';
            celestialBody.weight = 100;
            celestialBody.description = 'LALALALLA LALALALA';
            return request(host).put('update')
                .delay(1000)
                .payload(celestialBody)
                .send()
                .then((res) => {
                    celestialBody.id = res.body.id;
                    res.body.id = expect.stringMatching(UUID_REGEX);
                    res.body.createdAt = expect.stringMatching(DATE_REGEX);
                    res.body.updatedAt = expect.stringMatching(DATE_REGEX);
                    expect(res.body.name).toEqual(celestialBody.name);
                    expect(res.body.system).toEqual(celestialBody.system);
                    expect(res.body.description).toEqual(celestialBody.description);
                    expect(res.body.weight).toEqual(celestialBody.weight);
                    expect(res.body.category).toEqual('moon');
                });
        });

        test(`Confirm the update to the celestial body above`, () => {
            return request(host).get('getByUuid' + '?id=' + celestialBody.id)
                .delay(1500)
                .send()
                .then((res) => {
                    expect(res.body.id).toEqual(celestialBody.id);
                    res.body.id = expect.stringMatching(UUID_REGEX);
                    res.body.createdAt = expect.stringMatching(DATE_REGEX);
                    res.body.updatedAt = expect.stringMatching(DATE_REGEX);
                    expect(res.body.name).toEqual(celestialBody.name);
                    expect(res.body.system).toEqual(celestialBody.system);
                    expect(res.body.description).toEqual(celestialBody.description);
                    expect(res.body.weight).toEqual(celestialBody.weight);
                    expect(res.body.category).toEqual('moon');
                });
        });
    });

    // ================================================================

    describe(`Get all celestial bodies and check if the sum works correctly`, () => {
        test(`Successfully create a celestial body`, () => {
            const celestialBody2 = new CelestialBody();
            return request(host).post('create')
                .payload(celestialBody2)
                .send()
                .then((res) => {
                    celestialBody2.id = res.body.id;
                    res.body.id = expect.stringMatching(UUID_REGEX);
                    res.body.createdAt = expect.stringMatching(DATE_REGEX);
                    expect(res.body.name).toEqual(celestialBody2.name);
                    expect(res.body.system).toEqual(celestialBody2.system);
                    expect(res.body.description).toEqual(celestialBody2.description);
                    expect(res.body.weight).toEqual(celestialBody2.weight);
                    expect(res.body.category).toEqual(celestialBody2.category);
                });
        });

        test(`Get all`, () => {
            return request(host).get('getAll')
                .delay(500)
                .send()
                .then((response) => {
                    expect(response.body.length).toBeGreaterThanOrEqual(2)
                })
        });

        test(`Get getTotalWeight via endpoint`, () => {
            return request(host).get('getTotalWeight').delay(800)
                .send()
                .then((res) => {
                    expect(res.body[0]["sum(`weight`)"]).toBeGreaterThan(1);
                })
        });
    });

    // ================================================================

    describe(`Delete the celestial body}`, () => {
        test(`Successfully delete the celestial body`, () => {
            return request(host).delete('deleteByUuid' + '/' + celestialBody.id)
                .delay(2000)
                .send()
                .then((res) => {
                    expect(res.body.id).toEqual(celestialBody.id);
                    res.body.id = expect.stringMatching(UUID_REGEX);
                });
        });

        test(`Confirm the deletion of the celestialBody`, () => {
            return request(host).get('getByUuid' + '?id=' + celestialBody.id)
                .delay(2500)
                .send()
                .then((res) => {
                    expect(res.body).toEqual(null);
                });
        });
    });
});
