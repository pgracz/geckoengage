"use strict";
const superagent = require('superagent');
const debug = require('debug')('request');

const request = class Request {
    constructor(host) {
        this.hostname = host
        this.statusCode = 200;
        this.data = {};
        this.form = null;
        this.publicSuccess = false;
        this.headers = {};
        this.delayms = 0;
        this.retry = 1;
        this.outputForced = false;
        this.throwErrorOnFailure = false;
    }

    get(url) {
        this.method = 'GET';
        this.url = url;
        return this;
    }

    post(url) {
        this.method = 'POST';
        this.url = url;
        return this;
    }

    put(url) {
        this.method = 'PUT';
        this.url = url;
        return this;
    }

    delete(url) {
        this.method = 'DELETE';
        this.url = url;
        return this;
    }

    payload(data) {
        if (this.method !== 'GET' && this.method !== 'DELETE') {
            this.data = data;
        }

        return this;
    }

    setHeaders(headers) {
        this.headers = headers;

        return this;
    }

    delay(milliseconds) {
        this.delayms = milliseconds;

        return this;
    }

    send() {
        const url = this.hostname + '/' + this.url;
        debug(this.method + "ing to " + url);
        return this.sleep(this.delayms).then(() => {
            return new Promise((resolve, reject) => {
                const http = superagent(this.method, url);
                http.timeout({
                    response: 10000,
                    deadline: 30000,
                });
                if (this.retry) {
                    http.retry(this.retry);
                }

                for (let headerName of Object.keys(this.headers)) {
                    http.set(headerName, this.headers[headerName]);
                }
                if (this.form) {
                    http.type('form')
                    http.send(this.form);
                } else if (this.data) {
                    http.set('Content-Type', 'application/json');
                    http.send(JSON.stringify(this.data));
                }

                http.ok(() => true);
                http.end((err, res) => {
                    if (err) {
                        return reject(err);
                    }
                    if (this.throwErrorOnFailure && res.statusCode !== 200) {
                        this.logError(res);
                        return reject(new Error(this.throwErrorOnFailure));
                    }

                    if (this.outputForced) {
                        this.logError(res);
                    }
                    return resolve(res);
                })
            });
        })
    }

    // test() {
    //     return this.send().then((res) => {
    //         console.warn('res');
    //         console.warn(res);
        //     if (res.statusCode !== this.statusCode && !this.outputForced) {
        //         this.logError(res);
        //     }
        //
        //     if (this.statusCode > 399) {
        //         if (this.message) {
        //             expect(res.body.message).toEqual(this.message);
        //         }
        //         expect(res.body.code).toEqual(this.statusCode);
        //         expect(res.body.error).toEqual(true);
        //     }
        //     expect(res.statusCode).toEqual(this.statusCode);
        //     if (this.entity) {
        //         expect(res.body).toEqual(this.entity);
        //     }
        //     if (this.list || this.count) {
        //         // Check expected keys
        //         expect(res.body).toHaveProperty('results');
        //         expect(res.body).toHaveProperty('totalResults');
        //         expect(res.body).toHaveProperty('page');
        //         expect(res.body).toHaveProperty('pageSize');
        //         expect(res.body).toHaveProperty('totalPages');
        //
        //         // Check keys we can automatically check the value of
        //         expect(res.body.page).toEqual(this.payload.page || 1);
        //         expect(res.body.pageSize).toEqual(this.payload.pageSize || 25);
        //
        //         if (this.list) {
        //             expect(res.body.results).toEqual(this.list)
        //         }
        //         if (this.count) {
        //             expect(res.body.totalResults).toEqual(this.count)
        //         }
        //     }
        //     if (this.publicSuccess) {
        //         expect(res.body).toHaveProperty('success');
        //         expect(res.body.success).toBe(true);
        //     }
        //     if (this.deleteId) {
        //         expect(res.body).toHaveProperty('id');
        //         expect(res.body.id).toEqual(this.deleteId);
        //     }
        //     if (callback) {
        //         return callback(res.body, cookieParser.parse(res, {map: true}), res);
        //     }
        //     return res.body;
        // });
    // }

    logError(res) {
        console.log({
            url: this.hostname + '/' + this.url,
            requestBody: this.form ? this.form : JSON.stringify(this.data),
            requestCookies: this.cookies,
            responseCode: res.statusCode,
            responseBody: res.text.substring(0,1) === '{' ? res.body : res.text,
            responseHeaders: res.headers
        });
    }

    sleep (ms) {
        if (ms === 0) {
            return Promise.resolve();
        }
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, ms);
        });
    }
};

module.exports = (host) => {
    return new request(host);
};
