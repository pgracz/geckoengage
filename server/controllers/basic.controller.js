'use strict';

const BaseController = require('./base.controller');

module.exports = class BasicController extends BaseController {

    constructor(config, logger) {
        super(config.outputs.routesMap.BasicController);
        this.logger = logger.getLogger('BasicController');
        this.routesMap = config.outputs.routesMap.BasicController;
    }

    version(req, res) {
        this.logger.info("/version");

        return this.output(res, {
            name: process.env.npm_package_name, // eslint-disable-line no-process-env
            version: process.env.npm_package_version // eslint-disable-line no-process-env
        });
    }

};
