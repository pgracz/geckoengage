'use strict';

const HTTP_OK = 200;
const requiredConfigProperties = [
    'method',
    'route',
    'function'
];

module.exports = class BaseController {

    constructor(config) {
        config.map(BaseController._validateConfig);
        this.routesMap = config;
    }

    registerRoutes(server) {
        this.routesMap.forEach((item) => {
            server[item.method](item.route, this[item.function].bind(this));
        });
    }

    static _validateConfig(config) {
        if (config === null || config === undefined) { // eslint-disable-line no-undefined
            throw new Error("Config cannot be empty!");
        }

        requiredConfigProperties.forEach((item) => {
            if (!Object.prototype.hasOwnProperty.call(config, item)) {
                throw new Error(item + ' is a required parameter to create controller route.');
            }
        });
    }

    output(res, json) {
        res.writeHead(HTTP_OK, {
            'Content-Type': 'application/json'
        });
        res.end(JSON.stringify(json));
    }
};
