'use strict';

const BaseController = require('./base.controller');

module.exports = class PlanetController extends BaseController {

    constructor(config, logger, planetModel) {
        super(config.outputs.routesMap.PlanetController);
        this.logger = logger.getLogger('PlanetController');
        this.planetModel = planetModel;
    }

    addCelestialBody(req, res) {
        this.logger.debug('addCelestialBody was called.');

        return this.planetModel.create(req.body).then((data) => {
            return this.output(res, data);
        });
    }

    getCelestialBodyByUuid(req, res) {
        this.logger.debug('getCelestialBodyByUuid was called.');

        return this.planetModel.retrieve(req.query.id).then((data) => {
            return this.output(res, data);
        });
    }

    updateCelestialBody(req, res) {
        return this.planetModel.updateRecord(req.body).then((data) => {
            return this.output(res, data);
        });
    }

    deleteCelestialBody(req, res) {
        this.logger.debug('deleteCelestialBody was called.');

        return this.planetModel.erase(req.params.id).then((data) => {
            return this.output(res, data);
        });
    }

    getAllCelestialBodies(req, res) {
        this.logger.debug('getAllCelestialBodies was called.');

        return this.planetModel.retrieveAll().then((data) => {
            return this.output(res, data);
        });
    }

    getTotalWeightOfCelestialBodies(req, res) {
        this.logger.debug('getTotalWeightOfCelestialBodies was called.');

        return this.planetModel.getTotalWeight().then((data) => {
            return this.output(res, data);
        });
    }

};
