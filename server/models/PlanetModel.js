'use strict';

const AbstractCelestialModel = require('./AbstractCelestialModel');
const {v4: uuidv4} = require('uuid');
const tableName = 'planets';
const sumColumn = 'weight';

module.exports = class PlanetModel extends AbstractCelestialModel {

    constructor(storageService, logger) {
        super(storageService);
        this.logger = logger.getLogger('PlanetModel');
    }

    create(newPlanet) {
        return this.storageService.store(
            tableName,
            {
                id: uuidv4(),
                createdAt: new Date().toISOString(),
                name: newPlanet.name,
                system: newPlanet.system,
                description: newPlanet.description ? newPlanet.description : null,
                weight: newPlanet.weight,
                category: newPlanet.category
            }
        );
    }

    retrieve(uuid) {
        return this.storageService.getByUuid(tableName, uuid).then((data) => {
            return data === undefined ? null : data; // eslint-disable-line no-undefined
        });
    }

    erase(uuid) {
        return this.storageService.deleteByUuid(tableName, uuid);
    }

    updateRecord(updatedObject) {
        return this.storageService.update(
            tableName,
            {
                id: updatedObject.id,
                createdAt: updatedObject.createdAt,
                updatedAt: new Date().toISOString(),
                name: updatedObject.name,
                system: updatedObject.system,
                description: updatedObject.description ? updatedObject.description : null,
                weight: updatedObject.weight,
                category: updatedObject.category
            }
        );
    }

    retrieveAll() {
        return this.storageService.getAll(tableName);
    }

    getTotalWeight() {
        return this.storageService.getSum(tableName, sumColumn);
    }


};
