"use strict";

/* eslint-disable max-params,max-statements */
const knexConfig = require("../../knexfile").production;
const Knex = require('knex');
const ONE_SECOND = 1000;
const RETRIES = 10;

module.exports = class DBStorage {

    constructor(config, logger) {
        this.logger = logger.getLogger('DBStorage');
        this.config = config;

        this.runMigrations(RETRIES, ONE_SECOND)
            .catch((err) => {
                this.logger.error(err);
            })
            .then((res) => {
                this.logger.info(`Batch ${res[0]} run: ${res[1].length} migrations`);
                this.start();
            });

    }

    /*
     * Start DB connection and ensure events table exists
     */
    start() {
        this.logger.debug('start() was called.');
        this.db = new Knex({
            client: 'sqlite3',
            connection: {
                filename: this.config.db.sqlite
            }
        });
    }

    /**
     *
     * @param table
     * @param data
     */
    store(table, data) {
        return this.db(table).insert(data).then(() => {
            this.logger.info(data);
            return data;
        }).catch((err) => {
            this.logger.error(err);
            return err;
        });
    }

    getByUuid(table, uuid) {
        return this.db(table).where({
            id: uuid,
        }).select('*').then((result) => {
            this.logger.info(result[0]);
            return result[0];
        })
            .catch((err) => {
                this.logger.error(err);
                return err;
            });
    }

    deleteByUuid(table, uuid) {
        return this.db(table).where('id', uuid).del()
            .then((result) => {
                this.logger.info(result);
                return {id: uuid};
            })
            .catch((err) => {
                this.logger.error(err);
                return err;
            });
    }

    update(table, data) {
        return this.db(table).where({id: data.id})
            .update(data, ['id', 'createdAt', 'updatedAt', 'name', 'system', 'description', 'weight', 'category'])
            .then((result) => {
                this.logger.info(result);
                return data;
            })
            .catch((err) => {
                this.logger.error(err);
                return err;
            });
    }

    getAll(table) {
        return this.db.select().table(table).then((result) => {
            this.logger.info(result);
            return result;
        }).catch((err) => {
            this.logger.error(err);
            return err;
        });
    }

    getSum(table, column) {
        return this.db(table).sum(column).then((result) => {
            this.logger.info(result);
            return result;
        }).catch((err) => {
            this.logger.error(err);
            return err;
        });
    }

    async runMigrations(retries, delay) {
        for (let iteration = 1; iteration !== retries; iteration += 1) {
            try {
                return await new Knex(knexConfig).migrate.latest(); // eslint-disable-line no-await-in-loop
            } catch (err) {
                this.logger.info(err);
                await this.setTimeoutAsync(delay * iteration);// eslint-disable-line no-await-in-loop
            }
        }

        throw new Error(`Unable to run migrations after ${retries} attempts.`);
    }

    setTimeoutAsync(ms) {
        return new Promise((resolve) => {
            setTimeout(resolve, ms);
        });
    }

};
