"use strict";

const bodyParser = require('body-parser');
const dropEmpties = require('./middleware/dropEmpties.middleware');
const contentType = require('./middleware/appJsonType.middleware');

module.exports = class ModelRouter {
    constructor(server, log, controllers) {
        this.server = server;
        this.log = log.getLogger('router');

        server.getServer().use('/', bodyParser.json());
        server.getServer().use(dropEmpties);
        server.getServer().use(contentType);
        this.bindRoutes(controllers, server.getServer());
    }

    start(port) {
        return this.server.start(port);
    }

    bindRoutes(controllers, server) {
        controllers.forEach((controller) => {
            controller.registerRoutes(server);
        });
    }
};
