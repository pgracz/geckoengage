"use strict";

const http = require('http');
const DEFAULT_HTTP_PORT = 80;

module.exports = class WebServer {
    /**
     * Use a Polka instance to listen for http requests for events
     *
     * @param polka Instance of Polka webserver
     * @param log Instance of log4js
     */
    constructor(polka, log) {
        this.createServer = http.createServer;
        this.server = polka({});
        this.logger = log.getLogger('WebServer');
    }

    /**
     * Create a http server instance, and attach router to it (polka)
     * @param port Port to bind to
     * @returns {Promise<any>}
     */
    start(port) {
        if (typeof port === 'undefined') {
            port = DEFAULT_HTTP_PORT;
        }
        this.logger.debug('startServer() called for port: %s', port);
        return new Promise((resolve, reject) => {
            // Create server instance (http/https)
            const instance = this.createServer(this.server.handler);

            // Start server on specified port.
            return instance.once('error', (err) => {
                // Close the server on error
                instance.close();

                this.logger.error("Failed to start HTTP on port " + port);
                return reject(err);
            }).listen(port, '0.0.0.0', () => {
                this.logger.info("HTTP Webserver running on port " + port);
                return resolve(instance);
            });
        });
    }

    getServer() {
        return this.server;
    }

    // Proxies
    use(...args) {
        return this.server.use(...args);
    }

};
