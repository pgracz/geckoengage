"use strict";

module.exports = function dropEmpties(req, res, next) {

    if (req.body === null || req.body === undefined) { // eslint-disable-line no-undefined
        res.statusCode = 400;
        res.end('No body!');
    }
    next();
};
