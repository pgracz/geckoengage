"use strict";

module.exports = function appJsonType(req, res, next) {

    if (req.headers['content-type'] !== 'application/json') {
        res.statusCode = 400;
        res.end('Wrong content type');
    }
    next();
};
