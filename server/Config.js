"use strict";

/* eslint-disable no-sync, no-process-env, max-lines-per-function, max-statements */
const fs = require('fs');
const yaml = require('yaml');

module.exports = class Config {
    static getMergedConfig(filename) {
        let config = yaml.parse(fs.readFileSync(filename, 'utf8'));

        if (process.env.PORT) {
            config.outputs.http_server.port = process.env.PORT;
        }

        /* LOGGING */
        if (process.env.LOG_LEVEL) {
            config.logging.categories.default.level = process.env.LOG_LEVEL;
        }

        return config;
    }
};
