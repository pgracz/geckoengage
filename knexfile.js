'use strict'

// This should be moved to yaml config too.
module.exports = {
    production: {
        client: 'sqlite3',
        connection: {
            filename: './db/solarsystem.db'
        },
        migrations: {
            tableName: 'knex_migrations',
            directory: `${ __dirname }/db/migrations`
        }
    }
}
